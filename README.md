# Videolog PHP Class

Classe de comunicação em PHP com a API do [Videolog](http://videolog.tv/)

## Como usar:

### Inicialização: new Videolog($config)

O array `$config` leva 3 parametros: <br>
`token` o token que pode ser gerado [aqui](http://api.videolog.tv/usuario/chave) <br>
`usuario` Seu usuário no videolog <br>
`senha` Sua senha do videolog

```php
$videolog = new Videolog(array(
	'token' => $token
	'usuario' => $usuario
	'senha' => $senha
));
```

## Funcionalidades:

### search($query, $canal, $usuario, $qtdItens, $pagina)
Retorna o resultado de uma busca dentro do videolog

`$query` termo a ser buscado, separe as palavras por espaço, ex: “copa do mundo” <br>
`$canal` `opcional` (Buscar por vídeos dentro de um determinado canal) ID do canal a ser buscado <br>
`$usuario` `opcional` (Buscar por vídeos de um determinado usuário) ID do usuário <br>
`$qtdItens` `opcional` corresponde a quantidade de videos retornados por consulta, padrão em 25  <br>
`$pagina` `opcional` corresponde a página de itens, padrão em 1

```php
$busca = $videolog->search('copa do mundo', 2); // retornará os primeiros 25 videos do canal de Animação
$busca = $videolog->search('copa do mundo', 'Animação'); // retornará os primeiros 25 videos do canal de Animação
$busca = $videolog->search('copa do mundo', 'Animação', 25, 2); // retornará a segunda página, contendo 25 videos do canal de Animação, começando a contar a partir do 26º
```
#### Retorno
	stdClass Object (
		[total] => Quantidade de resultados para a busca
		[videos] => Array() com a lista de videos
	)

### getChannelID($canal)
Retorna o ID do canal

`$canal` pode receber tanto valor inteiro quanto string, se este valor for inteiro irá verificar se o ID corresponde a algum canal existente. <br>A lista completa de canais e ID's correspondentes pode ser acessada [clicando aqui](http://dev.videolog.tv/categorias-canais-api-videolog/#lib-canais)

```php
$canal = $videolog->getChannelID(2); // return int(2)
$canal = $videolog->getChannelID('Animação'); // return int(2)
```
#### Retorno
	int() - ID do canal

### getChannel($canal, $qtdItens, $pagina)
Retorna a listagem de video de um canal, também referido como categoria

`$canal` pode receber tanto valor inteiro quanto string, se este valor for inteiro deve ser correspondente ao ID do canal. A lista completa de canais e ID's correspondentes pode ser acessada [clicando aqui](http://dev.videolog.tv/categorias-canais-api-videolog/#lib-canais) <br>
`$qtdItens` `opcional` corresponde a quantidade de videos retornados por consulta, padrão em 25  <br>
`$pagina` `opcional` corresponde a página de itens, padrão em 1

#### Retorno
	stdClass Object (
		[paginas] => Quantidade de páginas neste canal
		[id] => ID do canal
		[videos] => Array() com a lista de videos
		[nome] => Nome do canal
	)


```php
$canal = $videolog->getChannel(2); // retornará os primeiros 25 videos do canal de Animação
$canal = $videolog->getChannel('Animação'); // retornará os primeiros 25 videos do canal de Animação
$canal = $videolog->getChannel('Animação', 25, 2); // retornará a segunda página, contendo 25 videos do canal de Animação, começando a contar a partir do 26º
```

### getUser($user, $pagina, $privacidade)
Retorna os dados de um usuário

`$user` ID ou nome do usuário <br>
`$pagina` `opcional` corresponde a página de itens, padrão em 1 <br>
`$privacidade` `opcional` 0 (vídeos liberados a todos os usuário), 1 (vídeos restritos a amigos), 2 (vídeos restritos ao usuário) e 6 (vídeos restritos aos sites do usuário)

```php
$usuario = $videolog->getUser(523240);
$usuario = $videolog->getUser('thevideos');
```
#### Retorno
	stdClass Object (
		[paginas] => Quantidade de páginas existentes
		[active] => 1 para ativo, 0 para inativo
		[videos] => Array() com a lista de videos
		[login] => Login do usuário
		[id] => ID do usuário
	)

### getFavorite($user, $pagina)
Retorna os videos favoritados de um usuário

`$user` ID ou nome do usuário <br>
`$pagina` `opcional` corresponde a página de itens, padrão em 1

```php
$favoritos = $videolog->getFavorite(523240);
```

#### Retorno
	stdClass Object (
		[paginas] => Quantidade de existentes
		[videos] => Array() com a lista de videos
		[login] => Login do usuário
		[id] => ID do usuário
	)

### addFavorite($videoID)
Favorita um video

`$videoID` ID do video que deseja favoritar

```php
$videolog->addFavorite(700910);
```

#### Retorno
	bool(true) Se o video for adicionado com sucesso

### getComments($videoID)
Retorna todos os comentários de um video

`$videoID` ID do video

```php
$comentarios = $videolog->getComments(700910);
```

#### Retorno
	Array(
		[x] => stdClass Object (
			[id_usuario] => ID do usuário que fez o comentário (0 se o usuário não for registrado)
			[texto] => Texto do comentário
			[data] => Data de criação do comentário ( YYYY-MM-DDThh:mm:ss )
			[id] => ID do comentário
			[nome] => Nome do usuário que fez o comentário
		)
	)

### getComment($commentID)
Retorna os dados de um comentário especifico

`$commentID` ID do comentário

```php
$comentario = $videolog->getComment(1889927);
```

#### Retorno
	stdClass Object (
		[id_usuario] => ID do usuário que fez o comentário (0 se o usuário não for registrado)
		[texto] => Texto do comentário
		[data] => Data de criação do comentário (YYYY-MM-DDThh:mm:ss)
		[id] => ID do comentário
		[nome] => Nome do usuário que fez o comentário
	)

### addComment($videoID, $texto)
Adiciona um comentário

`$videoID` ID do video que você irá comentar<br>
`$texto` texto do comentário

```php
$comentario = $videolog->addComment(700910, 'Comentário de teste');
```

#### Retorno
	bool(true)

### removeComment($commentID)
Remove um comentário

`$commentID` ID do comentario

```php
$videolog->removeComment(1889927);
```

#### Retorno
	bool(true)

### getVideo($videoID)
Retorna os dados de um video especifico

`$videoID` ID do video

```php
$video = $videolog->getVideo(700910);
```
#### Retorno
	stdClass Object (
		[ordem] => Ordenação dentro do videolog
		[id_canal] => ID do canal do video
		[thumb] => Link da thumb do video (JPG 268×200)
		[url_mp4] => Link do MP4 do video
		[duracao] => Tempo de duração do video (hh:mm:ss)
		[texto] => Descrição do video
		[visitas] => Quantidade de visitas
		[usuario_id] => ID do usuário que postou o video
		[criacao] => Data da criação do video (YYYY-MM-DDThh:mm:ss)
		[link] => Link do video no videolog
		[privacidade] => Indica por quem o video pode ser visto - 0 (todos), 1 (somente amigos) e 2 (somente o autor)
		[titulo] => Titulo do video
		[embed] => Tag HTML iFrame de embed do video
		[id] => ID do video
	)

### uploadVideo($path, $titulo, $descricao, $canal, $privacidade, $tags)
Faz o upload do video no videolog.

`$path` Path do arquivo do video <br>
`$titulo` Titulo do video <br>
`$descricao` Descrição do video <br>
`$canal` Canal do video <br>
`$privacidade` `opcional` 0 (vídeos liberados a todos os usuário), 1 (vídeos restritos a amigos), 2 (vídeos restritos ao usuário) e 6 (vídeos restritos aos sites do usuário). Padrão 0<br>
`$tags` `opcional` Array com as tags do video

```php
$upload = $videolog->uploadVideo('/home/videos/video.mp4', 'Titulo', 'Descrição', 2);
$upload = $videolog->uploadVideo('C:\videos\video.mp4', 'Titulo', 'Descrição', 2);
```	

#### Retorno
	stdClass Object (
		[ordem] => Ordenação dentro do videolog
		[id_canal] => ID do canal do video
		[thumb] => Link da thumb do video (JPG 268×200)
		[url_mp4] => Link do MP4 do video
		[duracao] => Tempo de duração do video (hh:mm:ss)
		[texto] => Descrição do video
		[visitas] => Quantidade de visitas
		[usuario_id] => ID do usuário que postou o video
		[criacao] => Data da criação do video (YYYY-MM-DDThh:mm:ss)
		[link] => Link do video no videolog
		[privacidade] => Indica por quem o video pode ser visto - 0 (todos), 1 (somente amigos) e 2 (somente o autor)
		[titulo] => Titulo do video
		[embed] => Tag HTML iFrame de embed do video
		[id] => ID do video
	)