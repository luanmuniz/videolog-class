<?php 

if(!class_exists('Videolog')){
	Class Videolog {

		const API = 'http://api.videolog.tv/';
		const FORMAT = 'json';
		var $token = NULL,
			$autenticacao = NULL;

		public function __construct($config){
			if(!isset($config['login']) || !isset($config['senha']) || !isset($config['token']))
				$this->erroLog('Complete the required fields for authentication');

			$this->token = $config['token'];
			$this->autenticacao = $this->authenticate($config['login'], $config['senha']);
		}

		private function makeRequest($url = NULL, $fields = array()){
			if($url === NULL) 
				return FALSE;
			
			$postLink = self::API.$url;

			$options = array(
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_FOLLOWLOCATION => TRUE,
				CURLOPT_ENCODING => '',
				CURLOPT_USERAGENT => 'spider',
				CURLOPT_SSL_VERIFYPEER => FALSE,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_CONNECTTIMEOUT => 0
			);

			if(!empty($fields)){

				if($fields['method'] == 'POST')
					$options[CURLOPT_POST] = TRUE;

				if($fields['method'] == 'DELETE')
					$options[CURLOPT_CUSTOMREQUEST] = 'DELETE';

				if(isset($fields['headers'])){
					$header = array();
					foreach($fields['headers'] AS $key => $value)
						$header[] = $key.': '.$value;

					$options[CURLOPT_HTTPHEADER] = $header;

					if($fields['method'] == 'POST')
						$options[CURLOPT_POSTFIELDS] = $fields['body'];

					if($fields['method'] == 'DELETE')
						$options[CURLOPT_POSTFIELDS] = '';

				} else {
					$options[CURLOPT_POSTFIELDS] = http_build_query($fields, NULL, '&');
					$options[CURLOPT_HTTPHEADER] = Array('application/x-www-form-urlencoded');
				}
			}

			$ch = curl_init($postLink);
				curl_setopt_array($ch, $options);
				$content = curl_exec($ch);
				$err     = curl_errno($ch);
				$errmsg  = curl_error($ch);
				$header  = curl_getinfo($ch);
			curl_close($ch);
			
			if($content === false)
				$this->erroLog("Request erro code ".$err.":".$errmsg);

			return $content;
		}

		public function erroLog($msg){
			echo '<pre>';
				print_r($msg);
			echo '</pre>';
			exit;
		}

		private function authenticate($login, $senha){
			$token = $this->makeRequest('usuario/login', array(
				'method' => 'POST',
				'login' => $login,
				'senha' => $senha
			));

			if($token == 'LOGIN OU SENHA INCORRETOS')
				$this->erroLog('Incorrect login or password');

			return str_replace('Autenticacao=', '', $token);
		}

		public function search($q = NULL, $canal = NULL, $usuario = NULL, $itens = 25, $page = 1){
			if($q === NULL || !is_string($q))
				return 'Fill out the search terms correctly';

			$parameters = "";
			if($canal) $parameters .= "&canal=".$this->getChannelID($canal);
			if($usuario) $parameters .= "&usuario=".$usuario;
			if($itens) $parameters .= "&itens=".$itens;
			if($page > 1) $parameters .= "&inicio=".($itens*($page-1));

			$return = json_decode($this->makeRequest('video/busca.'.self::FORMAT.'?q='.$q.$parameters));

			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			return $return->busca;
		}

		public function getChannelID($channel){
			$channelArray = array(
				'animacao' => 2,
				'animais' => 17,
				'artes' => 28,
				'autos-e-veiculos' => 3,
				'bebidas-e-gastronomia' => 27,
				'ciencia-e-tecnologia' => 18,
				'curtas-metragens' => 19,
				'educacao-e-instrucao' => 4,
				'entretenimento' => 5,
				'esportes' => 22,
				'eventos' => 30,
				'familia' => 7,
				'festas' => 6,
				'humor' => 10,
				'inovacao-e-empreendedorismo' => 29,
				'jornalismo' => 12,
				'moda-e-Beleza' => 26,
				'musicas' => 11,
				'politica' => 13,
				'seriados' => 20,
				'sexy' => 15,
				'viagens-e-lugares' => 23,
				'video-games' => 24,
				'videologger-e-webseries' => 25,
			);

			if(is_numeric($channel) && !in_array(settype($channel, 'integer'), $channelArray))
				return 'This ID doesn\'t belong to any category';

			if(is_string($channel)){
				$searchArray = Array(' ', 'ç', 'ã', 'í', 'ú', 'é', 'ê');
				$replaceArray = Array('-', 'c', 'a', 'i', 'u', 'e', 'e');
				$channel = str_replace($searchArray, $replaceArray, strtolower($channel));
				
				if(!array_key_exists($channel, $channelArray))
					return 'This category doesn\'t exist';

				$channel = $channelArray[$channel];
			}

			return $channel;
		}

		public function getChannel($channel, $itens = 25, $page = 1){
			$channelID = $this->getChannelID($channel);
			$return = json_decode($this->makeRequest('canal/'.$channelID.'/videos/'.$page.'.'.self::FORMAT.'?itens='.$itens));
			
			if(!is_object($return))
				return 'An error has occurred, notify the system administrator';

			return $return->canal;
		}

		public function getUser($user = NULL, $page = 1, $privacidade = 0){
			if($user === NULL)
				return 'Fill in the user name correctly';

			$return = json_decode($this->makeRequest('usuario/'.$user.'/videos/'.$page.'.'.self::FORMAT.'?privacidade='.$privacidade));

			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			return $return->usuario;
		}

		public function getFavorite($user = NULL, $pagina = 1){
			if($user === NULL)
				return 'Fill in the username correctly';

			$return = json_decode($this->makeRequest('usuario/'.$user.'/favoritos/'.$pagina.'.'.self::FORMAT));
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			unset($return->usuario->active);
			return $return->usuario;
		}

		public function addFavorite($videoID = NULL){
			if($videoID === NULL || !is_numeric($videoID))
				return 'Fill out the video ID properly';

			$teste = $this->getVideo($videoID);
			$xml = '<video><id>'.$videoID.'</id></video>';

			$return = $this->makeRequest('usuario/favorito', array(
				'method' => 'POST',
				'headers' => array(
					'Content-type' => 'text/xml',
					'Autenticacao' => $this->autenticacao,
					'Token' => $this->token
				),
				'body' => $xml
			));

			if($return === '')
				return TRUE;

			$return = json_decode($return);
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';
		}

		public function getComments($videoID = NULL){
			if($videoID === NULL || !is_numeric($videoID))
				return 'Fill out the video ID properly';			

			$return = json_decode($this->makeRequest('video/'.$videoID.'/comentarios.'.self::FORMAT));
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			return $return->video->comentarios;
		}	

		public function getComment($commentID = NULL){
			if($commentID === NULL || !is_numeric($commentID))
				return 'Fill out the comment ID properly';

			$return = json_decode($this->makeRequest('comentario/'.$commentID.'.'.self::FORMAT));
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			return $return->comentario;
		}

		public function addComment($videoID = NULL, $text = NULL){
			if($text === NULL || !is_string($text))
				return 'Fill out the comment text correctly';

			if($videoID === NULL || !is_numeric($videoID))
				return 'Fill out the ID of the video where the comment is inserted correctly';

			$xml = '<comentario><texto>'.$text.'</texto></comentario>';

			$return = $this->makeRequest('video/'.$videoID.'/comentario.'.self::FORMAT, array(
				'method' => 'POST',
				'headers' => array(
					'Content-type' => 'text/xml',
					'Autenticacao' => $this->autenticacao,
					'Token' => $this->token
				),
				'body' => $xml
			));

			$return = json_decode($return);
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			return $return->comentario;
		}

		public function removeComment($commentID = NULL){
			if($commentID === NULL || !is_numeric($commentID))
				return 'Fill out the ID of the comment correctly';

			$return = $this->makeRequest('comentario/'.$commentID.'.'.self::FORMAT, array(
				'method' => 'DELETE',
				'headers' => array(
					'Autenticacao' => $this->autenticacao,
					'Token' => $this->token
				)
			));

			if($return === '')
				return TRUE;

			$return = json_decode($return);
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';
		}

		public function getVideo($videoID = NULL){
			if($videoID === NULL || !is_numeric($videoID))
				return 'Fill out the video ID properly';

			$return = json_decode($this->makeRequest('video/'.$videoID.'.'.self::FORMAT));
			if(!is_object($return) || (isset($return->status) && $return->status == 'error'))
				return 'An error has occurred, notify the system administrator';

			// Remoção de variaveis inuteis do retorno
			unset($return->video->uri);
			unset($return->video->uri_videos_relacionados);
			unset($return->video->uri_comentarios);
			unset($return->video->mobile);
			unset($return->video->sinc);

			return $return->video;
		}

		public function getVideoID($data = array()){
			$xml = '<video>';
			$xml .= '<titulo>'.$data['titulo'].'</titulo>';
			$xml .= '<descricao>'.$data['descricao'].'</descricao>';
			$xml .= '<canal>'.$this->getChannelID($data['canal']).'</canal>';
			$xml .= '<privacidade>'.$data['privacidade'].'</privacidade>';

			if(isset($data['tags']) && !empty($data['tags'])){
				$xml .= '<tags>';
				foreach($data['tags'] AS $t)
					$xml .= '<tag>'.$t.'</tag>';
				$xml .= '</tags>';
			}

			if(isset($data['metatags']) && !empty($data['metatags'])){
				$xml .= '<metatags>';
				foreach($data['metatags'] AS $key => $value){
					$xml .= '<metatag>';
						$xml .= '<nome>'.$key.'</nome>';
						$xml .= '<valor>'.$value.'</valor>';
					$xml .= '<metatag>';
				}
				$xml .= '</metatags>';
			}

			$xml .= '</video>';

			$return = json_decode($this->makeRequest('video.'.self::FORMAT, array(
				'method' => 'POST',
				'headers' => array(
					'Content-type' => 'text/xml',
					'Autenticacao' => $this->autenticacao,
					'Token' => $this->token
				),
				'body' => $xml
			)));

			if(!isset($return->video->uuid))
				return false;

			return $return->video->uuid;
		}

		public function uploadVideo($path = NULL, $title = NULL, $desc = NULL, $channel = NULL, $private = 0, $tags = array(), $metatags = array()){
			if($path === NULL || $title === NULL || $desc === NULL || $channel === NULL) 
				return 'Fill in the fields correctly';

			if(!file_exists($path))
				return 'This file doesn\'t exist';

			$data = array(
				'titulo' => $title,
				'descricao' => utf8_encode($desc),
				'canal' => $channel,
				'privacidade' => $private,
				'tags' => $tags,
				'metatags' => $metatags
			);

			$uuid = $this->getVideoID($data);
			if(!$uuid)
				return 'An error has occurred, notify the system administrator';

			$fileContent = file_get_contents($path);
			if(!$fileContent)
				return 'An error has occurred, notify the system administrator';

			$boundary = $this->createBoundary();
			$body = '--'.$boundary."\r\n";
			$body .= 'Content-Disposition: form-data; name="video"; filename="video.mp4"'."\r\n\r\n"; 
			$body .= $fileContent."\r\n";
			$body .= '--'.$boundary.'--';

			$videolog_upload = $this->makeRequest('video/'.$uuid.'/upload', array(
				'method' => 'POST',
				'headers' => array(
					'Autenticacao' => $this->autenticacao,
					'Token' => $this->token,
					'content-type' => 'multipart/form-data; boundary='.$boundary
				),
				'body' => $body
			));

			$return = json_decode($videolog_upload);
			if(!isset($return->video))
				return 'An error has occurred, notify the system administrator';

			// Remoção de variaveis inuteis do retorno
			unset($return->video->uri);
			unset($return->video->uri_videos_relacionados);
			unset($return->video->uri_comentarios);
			unset($return->video->mobile);
			unset($return->video->sinc);

			return $return->video; 
		}

		public function createBoundary(){
			$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
			$boundary = '';
			for ( $i = 0; $i < 24; $i++ )
				$boundary .= substr($chars, $this->rand(0, strlen($chars) - 1), 1);

			return $boundary;
		}

		public function rand( $min = 0, $max = 0 ) {
			global $rnd_value;
			if ( strlen($rnd_value) < 8 ) {
				static $seed = '';
				$rnd_value = md5( uniqid(microtime() . mt_rand(), true ) . $seed );
				$rnd_value .= sha1($rnd_value);
				$rnd_value .= sha1($rnd_value . $seed);
				$seed = md5($seed . $rnd_value);
			}
			$value = substr($rnd_value, 0, 8);
			$rnd_value = substr($rnd_value, 8);
			$value = abs(hexdec($value));
			$max_random_number = 3000000000 === 2147483647 ? (float) "4294967295" : 4294967295;
			if ( $max != 0 )
				$value = $min + ( $max - $min + 1 ) * $value / ( $max_random_number + 1 );

			return abs(intval($value));
		}

	}
}